![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/datasata.JPG)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/slide-4.jpg)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Capture.PNG)
![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/91292168_brilliantovui_doc1147709639_i_1338_full.jpg)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Figure_0.png)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Figure_16.png)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Figure_17.png)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Figure_21.png)


![](https://gitlab.com/Antoniii/clustering-of-csv-files/-/raw/main/Figure_22.png)


## Sources

* [Tutorial for K Means Clustering in Python Sklearn](https://machinelearningknowledge.ai/tutorial-for-k-means-clustering-in-python-sklearn/)
* [mall_customers.csv](https://gist.githubusercontent.com/ryanorsinger/cb1222e506c1266b9cc808143ddbab82/raw/b2fe8213426159be7f9c8de108726d3d814153eb/mall_customers.csv)
* [2.3. Кластеризация](https://scikit-learn.ru/clustering/)
* [10.1. Кластеризация](https://education.yandex.ru/handbook/ml/article/klasterizaciya)
* [Как использовать Python для работы с кластеризацией данных](https://sky.pro/media/kak-ispolzovat-python-dlya-raboty-s-klasterizacziej-dannyh/)
* [Setting number of threads in python](https://stackoverflow.com/questions/73391779/setting-number-of-threads-in-python)